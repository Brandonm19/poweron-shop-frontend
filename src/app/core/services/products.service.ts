import { Product } from '../models/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from '../models/response';
@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  constructor(private http: HttpClient) {}

  public index(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>('/api/products');
  }
  public view(productId: string): Observable<ApiResponse> {
    return this.http.get<ApiResponse>('/api/products/view/' + productId);
  }
  public add(product: Product): Observable<ApiResponse> {
    return this.http.post<ApiResponse>('/api/products/add', { product });
  }
  public edit(productId: string, product: Product): Observable<ApiResponse> {
    return this.http.post<ApiResponse>('/api/products/edit/' + productId, { product });
  }
  public remove(productId: string): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>('/api/products/' + productId);
  }
}
